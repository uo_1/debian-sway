#!      /usr/bin/python3

import os
import subprocess as sp

home = os.path.expanduser('~')

print("*** Welcome to Debian Sway Install ***")

print("*** Installing aria2  dex  automake  bc  tree ***")
sp.call(["sudo","apt-get","install", "aria2","dex","automake","bc","tree"])

print("*** Installing aptitude ***")
sp.call(["sudo","apt-get","install", "aptitude"])

print("*** Installing  brightnessctl ***")
sp.call(["sudo","apt-get","install", "brightnessctl"])

print("*** Installing  build -essential , Skip this if you already installed it *** ")
sp.call(["sudo","apt-get","install", "build-essential"])

print("*** Installing  calcurse , cmatrix ***")
sp.call(["sudo","apt-get","install", "calcurse","cmatrix","cpp","cppcheck"])

print(" *** Installing  browsers ***")
sp.call(["sudo","apt-get","install", "chromium"])
sp.call(["sudo","apt-get","install", "falkon"])
sp.call(["sudo","apt-get","install", "firefox-esr"])


print("*** Installing  Notification deamon ***")
sp.call(["sudo","apt-get","install", "dunst"])
sp.call(["sudo","apt-get","install", "xfce4-notifyd"])

print("*** Installing  Fonts ***")
sp.call(["sudo","apt-get","install", "ttf-mscorefonts-installer"])
sp.call(["sudo","apt-get","install", "fonts-dejavu-core","fonts-droid-fallback","fonts-firacode","fonts-font-awesome","fonts-liberation"])
sp.call(["sudo","apt-get","install", "fonts-powerline","fonts-noto-core","fonts-noto-mono","fonts-roboto","fonts-ubuntu","fonts-anonymous-pro ","fonts-fantasque-sans"])
sp.call(["sudo","apt-get","install", "fonts-hack","fonts-hack-otf","fonts-hack-ttf","fonts-hack-web ","fonts-jetbrains-mono","fonts-noto"])
sp.call(["sudo","apt-get","install", "fonts-terminus","ttf-ubuntu-font-family"])

print("*** Installing  Calculator , gawk ***")
sp.call(["sudo","apt-get","install", "galculator","gawk"])

print("*** Installing  Text Editor  ***")
sp.call(["sudo","apt-get","install", "geany","geany-common","geany-plugins"])
sp.call(["sudo","apt-get","install", "gedit","gedit-plugins"])
sp.call(["sudo","apt-get","install","cherrytree "])
sp.call(["sudo","apt-get","install","vim","nano"])

print("*** Installing Polkit / Keyring ***")
sp.call(["sudo","apt-get","install", "gnome-keyring"])
sp.call(["sudo","apt-get","install", "policykit-1","mate-polkit "," mate-polkit-bin","mate-polkit-common "])

print(" *** Installing  Image Viewer ***")
sp.call(["sudo","apt-get","install", "gwenview"])

print("*** Installing  Screenshot  Utility ***")
sp.call(["sudo","apt-get","install", "grim","slurp "])
sp.call(["sudo","apt-get","install", " kde-spectacle "])

print("*** Installing  File Manager ***")
sp.call(["sudo","apt-get","install", "ntfs-3g","nautilus","nautilus-data","gvfs","gvfs-backends ","gvfs-common","gvfs-daemons","gvfs-libs"," nautilus-share","nautilus-wipe","python3-nautilus "])


print("*** Installing  System Monitor ***")
sp.call(["sudo","apt-get","install", "htop","psensor ","psensor-common ","psensor-server"])
sp.call(["sudo","apt-get","install", "neofetch"])

print("*** Installing  Password Manager ***")
sp.call(["sudo","apt-get","install", "keepass2 "," keepass2-doc "])

print("*** Installing  Themes ***")
sp.call(["sudo","apt-get","install", " lxappearance","materia-gtk-theme","papirus-icon-theme"])

print("*** Installing  Clipboard Support ***")
sp.call(["sudo","apt-get","install", "wl-clipboard","xsel"])

print("*** Sound Support ***")
sp.call(["sudo","apt-get","install", "pavucontrol","playerctl","pipewire ","pipewire-bin ","pipewire-doc ","pipewire-pulse","pulseaudio","pulseaudio-utils","sound-theme-freedesktop"])

print("*** Installing  Python Support ***")
sp.call(["sudo","apt-get","install", "python3","python-is-python3","python-pip","python3-xlib ","python3-wheel","python3-apt","python3-cairo","python3-dbus","python3-dev","python3-minimal","python3-distutils","python3-debian"])


## You can install your prefered terminal or  uncomment the terminal and installed a different terminal 
print("*** Choose your prefered  Terminal ***")
sp.call(["sudo","apt-get","install", "terminator"])
sp.call(["sudo","apt-get","install", "sakura"])
sp.call(["sudo","apt-get","install", "rxvt-unicode "])


print("*** Installing  Video Player *** ")
sp.call(["sudo","apt-get","install", "smplayer","smplayer-themes"])

print("***  Installing  Music Player ***")
sp.call(["sudo","apt-get","install", "audacious ","audacious-dev","audacious-plugins "])

print("***  Installing  PDF READER ***")
sp.call(["sudo","apt-get","install", "zathura","zathura-djvu","zathura-pdf-poppler"])

print("***  Installing  Zsh shell ***")
sp.call(["sudo","apt-get","install", "zsh","zsh-common"])

print("***  Installing  zip unzip ***")
sp.call(["sudo","apt-get","install", "zip","unzip","tar"])

print("***   Storage media Support   ***")
sp.call(["sudo","apt-get","install", "udisks2","udev","udevil ","udiskie "])

### Note : The package wofi is only in debian sid repository as of the time I'm writing this guide . 
### Note : If you are using debian stable you have to build wofi from source 
print("***   Wayland  /  Sway wm   ***")
sp.call(["sudo","apt-get","install", "suckless-tools","sway","swaylock","swaybg","sway-backgrounds","wayland-protocols","wdisplays","waybar"])
sp.call(["sudo","apt-get","install", "wofi","xwayland","xdg-desktop-portal","xdg-desktop-portal-gtk","xdg-desktop-portal-wlr"])


print("***   Other Essential Packages  ***")
sp.call(["sudo","apt-get","install", "xdg-user-dirs","xdg-utils","xz-utils","xxd"])

print("***   Virtual Machine Support  ***")

## Virt-Manager  
sp.call(["sudo","apt-get","install", "ovmf","qemu-kvm","libvirt-clients","libvirt-daemon-system","bridge-utils","virtinstall","libvirt-daemon","virt-manager"])


## print("Creating a directory")
## os.mkdir(home + "/Cursors")

print("Creating a directory")
os.mkdir(home + "/Screenshot")


##sp.call(["sudo","apt-get","install", "dunst","aria2","dex"])

sp.call(["bash",home + "/debian-sway/sway_install.sh"])
