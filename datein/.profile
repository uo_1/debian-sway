export MOZ_ENABLE_WAYLAND=1
export XDG_CURRENT_DESKTOP=sway
export XDG_SESSION_TYPE=wayland
export WLR_NO_HARDWARE_CURSORS=1
#export WLR_NO_HARDWARE_CURSORS=0
export WLR_RENDERER_ALLOW_SOFTWARE=1
export _JAVA_AWT_WM_NONREPARENTING=1
## For Android Studio ##
export STUDIO_JDK=/usr/lib/jvm/java-11-openjdk/
