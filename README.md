# Debian-Sway

This will be guide on Install debian with btrfs filesystem and then install sway wm or Kde Desktop. If you prefer that option.

## Debian Btrfs Installation Guide

This Guide was created for a simple purpose of making very easy to reinstall debian with btrfs filesystem using encryption .
Credits goes to those who I took inspiration from to create this guide.

## Reference

https://www.reddit.com/r/debian/comments/aa3zta/install_debian_to_a_btrfs_subvolume_and_not_the/

https://rootco.de/2018-01-19-opensuse-btrfs-subvolumes/

https://www.ordinatechnic.com/distribution-specific-guides/arch-linux/an-arch-linux-installation-on-a-btrfs-filesystem-with-snapper-for-system-snapshots-and-rollbacks

## Notice

Only install one display manager , I choose to include two in this guide to give other users choice .

- Ly Display Manager

- Sddm Display Manager

## Desktop Environment / Window Manager

This guide choose to use Kde as desktop environment for the Window Manager I choose sway wm.

Find out more about Window Manager or Desktop Environment :

https://wiki.archlinux.org/title/Window_manager

https://wiki.archlinux.org/title/Desktop_environment

https://github.com/swaywm/sway/wiki

https://wiki.debian.org/sway

https://wiki.archlinux.org/title/Wayland

## Requirements

This install changes Debian to the SID (Dev) Branch

- python 3

sudo apt install python python-is-python3

## Download Debian non-free netinstall

Use the following Debian ISO as the base https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/weekly-builds/amd64/iso-cd/

do NOT grab the EDU download and this includes non-free and firmware

## SwayWm Configuration

- Backup your default source.list

sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak

sudo cp sources.list /etc/apt/sources.list

sudo apt update

sudo apt upgrade

## Description

This Sway Configuration was inspirated by the https://github.com/EndeavourOS-Community-Editions/sway with a few adjustment to work with debian.

## Installation

git clone https://gitlab.com/uo_1/debian-sway.git

cd debian-sway

python3 debian-install.py

## Preview

<details>
    <summary><strong>Click me to preview</strong></summary>
    ```
            <img src="/datein/Preview/2022-01-29-14-03-42_grim.png">
    ```
</details>

## Credits and acknowledgment

https://www.reddit.com/r/debian/comments/aa3zta/install_debian_to_a_btrfs_subvolume_and_not_the/

https://rootco.de/2018-01-19-opensuse-btrfs-subvolumes/

https://www.ordinatechnic.com/distribution-specific-guides/arch-linux/an-arch-linux-installation-on-a-btrfs-filesystem-with-snapper-for-system-snapshots-and-rollbacks
