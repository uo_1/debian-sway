## This is a Guide to Setup Swaywm 

## References :  https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway 


**Notice** : The package** " wofi "**  is not in debian stable repository as of the time am writing this guide. Install wofi from **" debian sid repository "**.

## Important Information 

The software **" xfce4-notifyd "** requires some tweaking to use under wayland ( swaywm ) also the software **" flameshot " ** requires some tweaking to work under wayland ( swaywm ).  

The software **" wdisplay "** is in the AUR for Arch linux users 

## Installation 



## Debain 

sudo apt-get install mate-polkit gnome-keyring brightnessctl fonts-ubuntu fonts-firacode ttf-ubuntu-font-family fonts-terminus xfce4-notifyd dunst calcurse dex galculator gnome-keyring mate-polkit policykit-1 mate-polkit-bin mate-polkit-common geany geany-plugins gwenview grim slurp nautilus htop psensor lxappearance materia-gtk-theme papirus-icon-theme wl-clipboard pavucontrol playerctl terminator sakura smplayer smplayer-themes  audacious zathura zathura-djvu zathura-pdf-poppler  qpdfview xwayland xdg-desktop-portal xdg-desktop-portal-gtk xdg-desktop-portal-wlr waybar wdisplays wayland-protocols sway-backgrounds swaybg swaylock sway wofi  

## Arch 

sudo pacman -S mate-polkit gnome-keyring brightnessctl ttf-fira-code ttf-ubuntu-font-family xfce4-notifyd dunst geany geany-plugins dex calcurse  galculator gwenview grim slurp nautilus htop psensor lxappearance materia-gtk-theme  papirus-icon-theme  pavucontrol playerctl terminator sakura smplayer smplayer-themes  audacious zathura zathura-djvu  zathura-pdf-poppler qpdfview xorg-xwayland xdg-desktop-portal waybar sway swaylock swayidle wl-clipboard 

## Software 


- Policykit / Keyring : mate-polkit , gnome-keyring 

- Brightness : brightnessctl 

- WM :  sway , swaybg 

- Wayland compositor : wlroot 

- Wayland Support : xdg-desktop-portal-wlr 

- Essential Software : xdg-desktop-portal-gtk , xdg-desktop-portal , dex 

- Menu : wofi 

- Bar : waybar 

- Display : wdisplay 

- Lockscreen : swaylock 

- Xorg-Backend : xwayland - debian , xorg-xwayland -  arch 

- Notification : xfce4-notifyd , dunst 

- Fonts : Firacode , ubuntu-font 

- Image Viewer : gwenview 

- Screenshot : grim , slurp , flameshot 

- File Manager : nautilus , ranger 

- System Monitor : htop , psensor 

- Themes : lxappearance , materia-gtk-theme , papirus-icon-theme 

- Clipboard : wl-clipboard 

- Text-Editor : geany , geany-plugins 

- Terminal : terminator , sakura 

- Sound : pavucontrol , playerctl 

- Video Player : smplayer 

- Music Player : audacious 

- PDF READER : zathura , zathura-djvu , zathura-pdf-poppler , qpdfview  

- Calender : calcurse 

- Calculator : galculator 

## Documentation 

Read the documentation for help and to tweak your system to your liking. 

   References :  https://github.com/swaywm/sway/wiki/Useful-add-ons-for-sway 
                 https://github.com/jceb/dex 
		 https://github.com/swaywm/sway/issues/1423 

