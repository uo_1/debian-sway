#! /usr/bin/bash 

echo "Setting up Swaywm files"

DIRS=`pwd`

cp  -R  -v   $DIRS/datein/config/*      ~/.config/

cp -v $DIRS/datein/.profile      ~/.profile

cp -v $DIRS/datein/.gtkrc-2.0    ~/.gtkrc-2.0

chmod    -R +x          ~/.config/sway/scripts

chmod    -R +x        ~/.config/waybar/scripts  

echo  "Setting the wallpaper"

cp  -R  $DIRS/datein/Images/*       ~/Pictures

echo "Setting up Cursor theme"

cd $DIRS/datein/Cursor

tar xzvf DragonCursors.tar.gz  
tar xzvf Bibata-Modern.tar.gz   
cp -r DragonCursors    /usr/share/icons  
cp -r Bibata-Modern-Amber   Bibata-Modern-Classic   Bibata-Modern-Ice    /usr/share/icons   
cd ..

echo "Install fonts "

cd  /usr/share/fonts 

sudo   aria2c -x 4 -s 4 https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf 

sudo aria2c -x 4 -s 4 https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf 

sudo  aria2c -x 4 -s 4 https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf 
